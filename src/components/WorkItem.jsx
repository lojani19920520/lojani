import React from 'react'

export const WorkItem =  ({title,imgUrl,teach}) =>{
  return (
    <div className='bg-slate-300 dark:bg-slate-800 rounded-lg overflow-hidden shadow-lg hover:-translate-y-1  transition ' >
        
        <img className='w-full h-36  md:h-48  object-cover' src={imgUrl} alt={title} />
        <div className='text-gray-600 dark:text-gray-300 p-5 w-full'>
            <h3 className='text-left md:text-xl mb-2 md:mb-3 font-semibold'>{title}</h3>
            <p className='flex flex-wrap gap-2 flex-row items-center justify-start text-xs md:text-sm'>
                {teach.map(item => (
                    <span className='inline-block px-2 py-1 bg-slate-200 dark:bg-slate-900 rounded-md' key={item}>{item}</span>
                ))}
            </p>
        </div>
    </div>
  )
}

import React from "react";

import perfil from '../assets/perfil.jpg'
import {Tittle} from './Tittle';


export const Hero = () => {
  return (
    <div id="hero" className="flex items-center justify-between flex-col lg:flex-row  py-20 w-full h-[100vh]">
      {/* <div className="text-center">
        <h1 className="text-2xl md:text-4xl mb-1 md:mb-3 text-indigo-600 font-semibold dark:text-indigo-500">Hi, This Lojani</h1>
        <p className="text-md md:text-xl max-w-md mb-3 text-gray-600 dark:text-gray-300">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit, quidem.
        </p>
        <a href="#works" className="inline-block px-8 py-3 border border-transparent text-base font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 md:text-md">See Works</a>
      </div> */}
      <div>
        <Tittle>Octavio Lojani Morales Heranndez</Tittle>
        <h2 className="text-gray-600 dark:text-gray-200 font-semibold text-3xl mb-3">Universidad de nariño</h2>
        <p className="text-gray-500 dark:text-slate-400 text-2xl mb-5">Ingeniero de sistemas</p>
        <a href="#works" className=" w-full lg:w-auto text-center mb-8 inline-block px-8 py-3 border border-transparent text-base font-medium rounded-3xl text-white bg-indigo-600 hover:bg-indigo-700 md:text-md">Saber más</a>
      </div>

      <div className="">
        <img className="w-[200px] h-[200px] lg:w-[250px] lg:h-[250px] rounded-full object-cover" src={perfil} alt="/" />
       
      </div>
    </div>
  );
};

import React from 'react'



export const LenguajeItem =({name, imgUrl})=> {
  return (
    <div className='bg-slate-300 dark:bg-slate-800 rounded-lg overflow-hidden shadow-lg'>
        <img className='w-full h-28 object-cover' src={imgUrl} alt="no Image" />
        <h3 className='text-xs py-2 text-center text-gray-600 dark:text-gray-300'>{name}</h3>
      
    </div>
  )
}

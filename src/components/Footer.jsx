import React from "react";
import {
  FaFacebook,
  FaGithub,
  FaInstagram,
  FaTwitter,
  FaTwitch,
  FaYoutube
} from "react-icons/fa";

import { Tittle } from "./Tittle";

export const Footer = () => {
  return (
    <div id="contact">
      <div className="w-full md:w-6/12">
        <Tittle>Contacto</Tittle>
      </div>
      <h2></h2>

      <div className="py-5 bg-slate-800 text-center text-gray-300 rounded-t-lg">
        <div className="flex md:flex-row flex-col justify-center items-center gap-12 col-span-2 pt-8 md:pt-2">
          <div>
            <a
              href="mailto:lojani19920520@gmail.com"
              className=" text-sm md:text-md hover:text-indigo-500"
            >
              lojani19920520@gmail.com
            </a>
            <p className="text-xs  mt-2 text-gray-500">
              Lojani Morales {new Date().getFullYear()}.Todos los derechos reservados
            </p>
          </div>

          <div>
            <p className="font-bold uppercase mb-2">Redes Sociales</p>
            <div className="flex justify-center gap-4 text-2xl col-span-2 pt-8 md:pt-2">
              <a href="#"><FaFacebook /></a>
              {/* <FaInstagram /> */}
              <FaTwitter />
              <FaTwitch />
              <FaGithub />
              <a href="https://www.youtube.com/channel/UC2CHS5upDWGiPSyraZK8NSg"><FaYoutube/></a>
            </div>
          </div>

          <div className="col-span-2 pt-8 md:pt-2">
            <p className="font-bold uppercase mb-2">subscribete a nuestra pagina</p>
           
            <form className="flex flex-col sm:flex-row">
              <input className="w-full p-2 mr-2 rounded mb-4" type="email" />
              <button className="p-2 mb-4">Subscribete</button>
            </form>
          </div>
        </div>
      </div>
     
    </div>
  );
};

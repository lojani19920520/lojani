import React from 'react'
import services from '../data/services'
import {ServiceItem} from './ServiceItem'
services
import { Tittle } from './Tittle'


export const Services =()=> {
  return (
    <div id='servicios' className='py-12'>
        <Tittle>Nuestros Servicios</Tittle>
        <div className='grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-5'>
          {services.map(service=>(
            <ServiceItem
              key={service.title}
              title={service.title}
              icon={service.icon}
              description={service.description}
            />
          ))}
        </div>
    </div>
  )
}

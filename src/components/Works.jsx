import React from 'react'
import works from '../data/works';

import {Tittle} from './Tittle';
import {WorkItem} from './WorkItem';

export const Works=() => {
  return (
    <div id='proyectos' className='py-12'>
      <Tittle >Trabajos Recientes</Tittle>
      <div className='grid grid-cols-1 md:grid-cols-3 gap-5'>
        {works.map(work => (
          <WorkItem 
            key={work.title}
            title={work.title}
            imgUrl={work.imgUrl}
            teach={work.teach}
          />
        ))}
      </div>
    </div>
  )
}

import React from "react";

import { Tittle } from "./Tittle";

import lenguajes from "../data/lenguajes";
import { LenguajeItem } from "./LenguajeItem";

export const About = () => {
  return (
    <div id="about" className="py-12">
      <Tittle>Sobre mi</Tittle>
      <div className="text-gray-200 lg:flex-row flex-col flex justify-between gap-8">
        <div  className="w-full">
          <h3 className="text-gray-600 dark:text-gray-300 text-xl font-semibold text-center py-6">
            Habilidades
          </h3>
          <p className="text-lg text-gray-500 dark:text-slate-400">
            Soy una persona apasionada por la tecnologia en especial todo lo que tiene que ver con programación,
            me gusta ser constante en las objetivos y metas que me propongo y estar en constante aprendizaje para 
            mejorar y adquirir experiencia que me ayude a ser mejor persona y profesional.en la actualidad estoy
            por culminar una etapa muy importante en mi vida que me ha enseñado a valorar mucho a las personas con las
            que cuento y las que han sido parte de este proceso directa e indirectamente me caracterizo por ser cumplido 
            en los compromisos y responsabilidades que adquiro al momento de realizar cualquier labor que se me sea asignada.
            <br />
            <br />
            acontinuación presento algunas de las tecnologias con las que he tenido la oportunidad de trabajar en algunos proyectos personales como laborales independientes.
          </p>
        </div>
        <div className="w-full">
          <h3 className="text-center text-gray-600 dark:text-gray-300 font-semibold text-xl py-6">
            Tecnologias Utilizadas
          </h3>
          <div className="grid lg:grid-cols-4 grid-cols-2 justify-center items-center gap-3">
            {lenguajes.map((lenguaje) => (
              <LenguajeItem
                key={lenguaje.name}
                name={lenguaje.name}
                imgUrl={lenguaje.imgUrl}
              />
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};

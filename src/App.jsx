import React, { useState, useEffect } from "react";

import { VscColorMode } from "react-icons/vsc";

import onda from './assets/wave.svg';

import { Hero } from "./components/Hero";
import { Services } from "./components/Services";
import { Works } from "./components/Works";
import { About } from "./components/About";
import { Footer } from "./components/Footer";
import { Navbar } from "./components/Navbar";

export const App = () => {
  const [theme, setTheme] = useState(null);

  useEffect(() => {
    if (window.matchMedia("(prefers-color-scheme:dark)").matches) {
      setTheme("dark");
    } else {
      setTheme("light");
    }
  }, []);

  useEffect(() => {
    if (theme == "dark") {
      document.documentElement.classList.add("dark");
    } else {
      document.documentElement.classList.remove("dark");
    }
  });

  const handleTheme = () => {
    setTheme(theme === "dark" ? "ligh" : "dark");
  };

  return (
    <>
      <button
        type="buttom"
        onClick={handleTheme}
        className="fixed z-20 right-2 bg-slate-400 text-white top-4  text-lg p-1 rounded-md"
      >
        <VscColorMode />
      </button>
      <div className="font-inter bg-white dark:bg-slate-900">
      
      
     
        <Navbar />
        <img className="absolute lg:z-0 md:-z-10 -z-10  top-6 right-0" src={onda} alt="" />
        <div className="max-w-5xl mx-auto w-11/12 ">
          <Hero />
          <Services />
          <Works />
          <About />
          <Footer />
          <br />
        </div>

      </div>
      
    </>
  );
};

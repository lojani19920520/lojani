
import img1 from '../assets/appbelleza.png';
import img2 from '../assets/Pantallaregistro.png';
import img3 from '../assets/movil.png';
import img4 from '../assets/social.png';
import img5 from '../assets/p1.jpg';

export default [
    {
        title:'diseño app figma',
        imgUrl:img1,
        teach:['HTML','CSS']
    },
    {
        title:'sitio web',
        imgUrl:img2,
        teach:['HTML','CSS','JS']
    },
    {
        title:'App movil flutter',
        imgUrl:img3,
        teach:['FLUTTER','DART','NODE']
    },
    {
        title:'project mysql',
        imgUrl:img4,
        teach:['MYSQL','ANGULAR','TAILWIND']
    },
    {
        title:'project mysql2',
        imgUrl:img5,
        teach:['MYSQL','ANGULAR','TAILWIND']
    },
    {
        title:'project mysql3',
        imgUrl:img1,
        teach:['MYSQL','ANGULAR','TAILWIND']
    },
    {
        title:'project mysql4',
        imgUrl:img2,
        teach:['MYSQL','ANGULAR','TAILWIND']
    },
    {
        title:'project mysql5',
        imgUrl:img5,
        teach:['MYSQL','ANGULAR','TAILWIND']
    },
    {
        title:'project mysql6',
        imgUrl:img2,
        teach:['MYSQL','ANGULAR','TAILWIND']
    },
    
]
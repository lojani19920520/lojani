import css from '../assets/l1.png';
import html from '../assets/l2.png';
import js from '../assets/l3.png';
import mysql from '../assets/l4.jpg';
import postgres from '../assets/l5.png';
import react from '../assets/l6.png';
import laravel from '../assets/l7.png';
import angular from '../assets/l8.png';
import tsc from '../assets/l9.png';
import express from '../assets/10.png';
import tailwind from '../assets/l10.png';
import bootstrap from '../assets/l11.jpg';



export default  [
    {
        name:'HTML',
        imgUrl:html
    },
    {
        name:'CSS',
        imgUrl:css
    },
    {
        name:'JS',
        imgUrl:js
    },
    {
        name:'MYSQL',
        imgUrl:mysql
    },
    {
        name:'POSTGRES',
        imgUrl:postgres
    },
    {
        name:'REACT',
        imgUrl:react
    },
    {
        name:'LARAVEL',
        imgUrl:laravel
    }
    ,
    {
        name:'ANGULAR',
        imgUrl:angular
    },
    {
        name:'TSC',
        imgUrl:tsc
    },
    {
        name:'EXPRESS',
        imgUrl:express
    },
    {
        name:'TIALWIND',
        imgUrl:tailwind
    },
    {
        name:'BOOTSTRAP',
        imgUrl:bootstrap
    }
]
import {MdWeb} from 'react-icons/md'
import {FaHandHoldingHeart} from 'react-icons/fa';
import {VscCode} from 'react-icons/vsc';


export default [
    {
        title:'Web Design',
        description:'Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur, dolores.',
        icon:<MdWeb className='w-full h-full'/>  
    },
    {
        title:'Ui Design',
        description:'Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur, dolores.',
        icon:<FaHandHoldingHeart className='w-full h-full'/>  
    },
    {
        title:'Web Development',
        description:'Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur, dolores.',
        icon:<VscCode className='w-full h-full'/>  
    }
]